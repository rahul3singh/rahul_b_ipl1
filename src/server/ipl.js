exports.team_Won_Toss_And_Matches = function (matches) {
  // By using Higher order function

  const output = matches.filter((val)=>{
    if(val.toss_winner === val.winner){
      return true
    }
  })
  .map((data)=>{
    return data.winner
  })
  .reduce(function (acc, curr) {
  if (typeof acc[curr] == 'undefined') {
    acc[curr] = 1;
  } else {
    acc[curr] += 1;
  }

  return acc;
}, {});

  return output



  // By using for loops

  // const result = {};
  // for (let match of matches) {
  //   if (match.toss_winner === match.winner) {
  //     if (result[match.toss_winner]) {
  //       result[match.toss_winner] += 1;
  //     } else {
  //       result[match.toss_winner] = 1;
  //     }
  //   }
  // }
  // console.log(result);
  // return result;


};



exports.player_Of_The_Match_Per_Season = function (matches) {

  //  Using higher order function

  const finalData = matches.reduce((acc, curr)=>{
    if(acc[curr.season]){
      if(acc[curr.season][curr.player_of_match]){
        acc[curr.season][curr.player_of_match] +=1
      }
      else{
        acc[curr.season][curr.player_of_match] = 1
      }
    }
    else{
      acc[curr.season] = {};
    }
    return acc
  }, {});
  const output = Object.entries(finalData).map((val)=>{
    const most = Object.keys(val[1]).reduce((a, b) => val[1][a] >= val[1][b] ? a : b);
    return [val[0], most]
  })
  // console.log(output)
  return output


  // Using for loops
  // const final = {};
  // const year = matches.map(match => match.season).filter((season, index, matchSeasons) => {
  //       return matchSeasons.indexOf(season) === index;
  //   }).sort();
  // // console.log(year);
  // for (let i of year) {
  //   let temp = {};
  //   for (let match of matches) {
  //     if (match.season === i) {
  //       if (temp[match.player_of_match]) {
  //         temp[match.player_of_match] += 1;
  //       } else {
  //         temp[match.player_of_match] = 1;
  //       }
  //     }
  //   }
  //   let max;
  //   let k = 0;
  //   for (let i in temp) {
  //     if (k < temp[i]) {
  //       k = temp[i];
  //       max = i;
  //     }
  //   }
  //   final[i] = max;
  // }
  // console.log(final)
  // return final;

  }



  exports.strike_Rate_Of_A_Batsman_In_Each_Season = function (
  matches,
  deliveries
) {
    const get = matches.reduce((a, c)=>{
    const second = deliveries.reduce((acc, curr)=>{
      if(curr.match_id == c.id && curr.batsman == 'MS Dhoni'){
        if(acc[c.season]){
          acc[c.season] = [acc[c.season][0], acc[c.season][1]+parseInt(curr.batsman_runs)]
          if((parseInt(curr.wide_runs) + parseInt(curr.noball_runs))===0){
              acc[c.season] = [acc[c.season][0]+=1, acc[c.season][1]]
          }
        }
        else{
          acc[c.season] = [0, parseInt(curr.batsman_runs)]
             if((parseInt(curr.wide_runs) + parseInt(curr.noball_runs))===0){
              acc[c.season] = [1, acc[c.season][1]]
             }
        }
      }
      return acc
    }, {})
    const key = Object.keys(second)
    const year = key[0]
    if(second[year] != undefined){
    if(a[year]){
      a[year] = [a[year][0]+second[year][0], a[year][1]+second[year][1]]
    }
    else{
      a[year] = [second[year][0], second[year][1]] 

    }
  }
    return a
  }, {})
    // console.log(get)
    const dataInto2d = Object.entries(get)
    const finalData = dataInto2d.map((d)=>{
      // console.log(d[1][0])
     let calc = ((d[1][1]/d[1][0])*100).toFixed(2)
     return [d[0], calc]
    })
    // console.log(finalData)
    return finalData

 

};


exports.best_Economy_In_Super_Overs = function (deliveries) {

  // By using higher order functions

  const output = deliveries.filter((del)=>{
    return parseInt(del.is_super_over)
  }).reduce((acc, curr)=>{
    if(acc[curr.bowler]){
      acc[curr.bowler] = [acc[curr.bowler][0], acc[curr.bowler][1]+=(parseInt(curr.batsman_runs)+parseInt(curr.noball_runs)+parseInt(curr.wide_runs))]
     if((parseInt(curr.wide_runs) + parseInt(curr.noball_runs))=== 0){
        acc[curr.bowler] = [acc[curr.bowler][0]+=1, acc[curr.bowler][1]]
      }
     
    }
    else{
        acc[curr.bowler] = [0,(parseInt(curr.batsman_runs)+parseInt(curr.noball_runs)+parseInt(curr.wide_runs))]
      if((parseInt(curr.wide_runs) + parseInt(curr.noball_runs))=== 0){
        acc[curr.bowler] = [1, acc[curr.bowler][1]]
      }
    }
    return acc
  }, {})
  const dataInto2d = Object.entries(output).reduce((acc, curr)=>{
    acc[curr[0]] = ((curr[1][1]/ (curr[1][0]/6).toString()).toFixed(2))
    return acc
  }, {})
  const sorted = Object.entries(dataInto2d)
  sorted.sort((a,b)=>{
    return parseInt(a[1])-parseInt(b[1])
  })

  return sorted[0]



  //  By using for loops 

//   const result = {};
//   const balls = {};
//   const runs = {};
//   for (let del of deliveries) {
//     let super_Over_Check = parseInt(del.is_super_over);
//     if (super_Over_Check) {
//       if (parseInt(del.wide_runs) === 0 && parseInt(del.noball_runs) === 0) {
//         if (balls[del.bowler]) {
//           balls[del.bowler] += 1;
//         } else {
//           balls[del.bowler] = 1;
//         }
//       }

//       if (runs[del.bowler]) {
//         let calc =
//           parseInt(del.batsman_runs) +
//           parseInt(del.noball_runs) +
//           parseInt(del.wide_runs);
//         runs[del.bowler] += calc;
//       } else {
//         let calc =
//           parseInt(del.batsman_runs) +
//           parseInt(del.noball_runs) +
//           parseInt(del.wide_runs);
//         runs[del.bowler] = calc;
//       }
//     }
//   }
//   for (let i in runs) {
//     let economy = (runs[i] / (balls[i] / 6)).toFixed(2);
//     result[i] = economy;
//   }

//   let entries = Object.entries(result);
//   let sorted = entries.sort((a, b) => {
//     return a[1] - b[1];
//   });
//   // console.log(sorted)
//   const finalData = {};
//   for (let i of sorted) {
//     finalData[i[0]] = i[1];
//   }
//   console.log(finalData)
//   return finalData;

};


exports.player_Dismissed_By_Another_Player = (deliveries) => {

  // By using higher order function

  const output = deliveries.filter((del)=>{
    if(del.player_dismissed === 'MS Dhoni'){
      return true
    }
  })
  .map((data)=>{
    return data.bowler
  })
  .reduce(function (acc, curr) {
  if (typeof acc[curr] == 'undefined') {
    acc[curr] = 1;
  } else {
    acc[curr] += 1;
  }

  return acc;
}, {});
  let finalData = Object.entries(output).sort((a, b)=>{
    return b[1] -a[1]
  }).slice(0, 10).reduce((acc, curr)=>{
    acc[curr[0]] = curr[1]
    return acc
  }, {})
  // console.log(finalData);
  return finalData



  // By using For loop 

  // const result = {};
  // const player_Name = "MS Dhoni";
  // for (let del of deliveries) {
  //   if (del.player_dismissed == "MS Dhoni") {
  //     if (result.hasOwnProperty(del.bowler)) {
  //       result[del.bowler] += 1;
  //     } else {
  //       result[del.bowler] = 1;
  //     }
  //   }
  // }

  // // console.log(result)
  // let check = Object.entries(result);
  // let sorted = check.sort((a, b) => {
  //   return b[1] - a[1];
  // });
  // // console.log(check)
  // sorted = sorted.slice(0, 10);
  // const finalData = {};
  // for (let i of sorted) {
  //   finalData[i[0]] = i[1];
  // }
  // console.log(finalData);
  // return finalData;



};




