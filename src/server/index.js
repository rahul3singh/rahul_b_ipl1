const fs = require("fs")
const csv = require("csvtojson");
const MATCHES_FILE_PATH = "../data/matches.csv";
const DELIVERIES_FILE_PATH = "../data/deliveries.csv";
const myModule = require('./ipl.js');
const team_Won_Toss_And_Matches = myModule.team_Won_Toss_And_Matches;
const player_Of_The_Match_Per_Season = myModule.player_Of_The_Match_Per_Season;
const strike_Rate_Of_A_Batsman = myModule.strike_Rate_Of_A_Batsman_In_Each_Season;
const best_Economy_In_Super_Overs = myModule.best_Economy_In_Super_Overs;
const most_Dismissed_A_Batsman_By_A_Bowler = myModule.player_Dismissed_By_Another_Player;
const JSON_OUTPUT_FILE_PATH = "../public/output";

function main(){
    csv()
    .fromFile(MATCHES_FILE_PATH)
    .then(matches =>{
        csv()
        .fromFile(DELIVERIES_FILE_PATH)
        .then(deliveries=>{
             // console.log(matches.slice(0, 5))
            let team_Won_Toss_And_Match = team_Won_Toss_And_Matches(matches)
            // console.log(team_Won_Toss_And_Match)
            save_All_Data(team_Won_Toss_And_Match, "team_Won_Toss_And_Matches");
            let awards = player_Of_The_Match_Per_Season(matches);
            save_All_Data(awards, "player_Of_The_Match_Per_Season")
            let strike = strike_Rate_Of_A_Batsman(matches, deliveries)
            save_All_Data(strike, "strike_Rate_Of_MS_Dhoni__In_Each_Season")
            let super_over = best_Economy_In_Super_Overs(deliveries)
            save_All_Data(super_over, "best_Economy_In_Super_Overs")
            let most_Dismissed = most_Dismissed_A_Batsman_By_A_Bowler(deliveries)
            save_All_Data(most_Dismissed, "most_Dismissed_Ms_dhoni_By_A_Bowler");
        })     
    })
}


function save_All_Data(value, key){
   const jsonData = {
     [key]: value
   }
   fs.writeFile(JSON_OUTPUT_FILE_PATH+`/${key}.json`, JSON.stringify(jsonData), "utf8", (err)=>{
       if(err){
           console.log(err)
       }
   })
}

main();